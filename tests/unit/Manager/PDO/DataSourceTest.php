<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Test;

use PHPUnit\Framework\TestCase;
use Spinit\DataSource\Manager\PDO\DataSource;
use Spinit\Util;
/**
 * Description of DataSourceTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSourceTest extends TestCase
{
    /**
     *
     * @var DataSource
     */
    private $object;
    
    public function setUp()
    {
        $this->object = DataSource::get(Util\getenv("TEST_UNIT_CONNECTION"));
        $this->assertNotEmpty($this->object->getCommandFactory());
    }
    public function testInit()
    {
        $this->assertNotEmpty($this->object);
        $this->assertTrue($this->object->connect());
        $this->assertNotEmpty($this->object->getCore());
    }
    public function testAlign()
    {
        $this->object->drop('t11');
        $this->assertFalse($this->object->check('t11'));
        $fields = [
            'id'=>['type'=>'integer', 'pkey'=>'1'],
            'nme'=>['type'=>'varchar', 'size'=>'50']
        ];
        $this->assertTrue($this->object->align(new \ArrayObject(['name'=>'t11', 'fields'=>$fields])));
        $this->assertTrue($this->object->check('t11'));
        $this->assertTrue($this->object->check('t11.id'));
        $this->assertTrue($this->object->check('t11.nme'));
        $this->assertFalse($this->object->check('t11.dsc'));
    }
    
    public function testAlignAdd()
    {
        $this->assertTrue($this->object->align(new \ArrayObject(['name'=>'t11', 'fields'=>['dsc'=>['type'=>'varchar', 'size'=>'10']]])));
        $this->assertTrue($this->object->check('t11.dsc'));
    }
    
    public function testDataModify()
    {
        $this->object->insert("t11", ['id'=>1, 'nme'=>'ciao']);
        $rec = $this->object->first('t11', '1');
        $this->assertEquals("ciao", $rec['nme']);
    }
}