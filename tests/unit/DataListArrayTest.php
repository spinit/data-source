<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Test;

use PHPUnit\Framework\TestCase;
use Spinit\DataSource\DataListArray;

/**
 * Description of DataListArrayTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataListArrayTest extends TestCase
{
    private $object;
    
    public function setUp()
    {
        $this->object = new DataListArray([[1,2], [3,4]]);
    }
    
    public function test1()
    {
        $this->assertEquals([1, 2], $this->object->first());
        $this->assertFalse($this->object->valid());
    }
}
