<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Test;

use PHPUnit\Framework\TestCase;
use Spinit\DataSource\DataSource;

/**
 * Description of DataSourceTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSourceTest extends TestCase
{
    
    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        DataSource::register('test', __NAMESPACE__.':DSTest', ['param1'=>'1', 'param2'=>'2']);
    }
    
    public function testCreation()
    {
        $ds = DataSource::get('test:my:string:config');
        $this->assertEquals('TEST OK', $ds->getType());
        $this->assertEquals('test', $ds->getInfo(0));
        $this->assertEquals('config', $ds->getInfo(3));
    }
    
    public function testParam()
    {
        $ds = DataSource::get('test:un:altro', ['param2'=>'--', 'param3'=>'3']);
        $this->assertEquals('1', $ds->getParam('param1'));
        $this->assertEquals('--', $ds->getParam('param2'));
        $this->assertEquals('3', $ds->getParam('param3'));
    }
    
    public function testLastID()
    {
        $ds = DataSource::get('test:20');
        $this->assertEquals('20', $ds->getLastID());
    }
}


class DSTest extends \Spinit\DataSource\DataSource
{
    public function align(\ArrayAccess $struct) {
        
    }

    public function check($name) {
        
    }

    public function connect() {
        
    }

    public function delete($resource, $pkey) {
        
    }

    public function drop($name) {
        
    }

    public function exec($query, $param, $args = array(), $info = array()) {
        
    }

    public function find($resource, $cond, $fields) {
        
    }

    public function first($resource, $pkey, $fields = '') {
        
    }

    public function getCommandLast() {
        
    }

    public function getCommandList($reset = false) {
        
    }

    public function getType()
    {
        return 'TEST OK';
    }

    public function insert($resource, $data) {
        
    }

    public function load($query, $param, $args = array(), $info = array()) {
        
    }

    public function update($resource, $data, $pkey) {
        
    }

    public function getLastID()
    {
        return $this->getInfo(1);
    }

    public function getCore() {
        
    }

    public function quote($string, $wrap = true) {
        
    }

    public function hex($str) {
        
    }

    public function unhex($str) {
        
    }

}