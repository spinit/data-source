<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Type;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface DataSourceInterface
{
    public function init($info, $param = []);
    
    public function getType();
    public function align(\ArrayAccess $struct);
    public function check($name);
    public function drop($name);
    
    public function quote($string, $wrap = true);
    public function hex($str);
    public function unhex($str);
    
    public function load($query, $param, $args = [], $info = []);
    public function exec($query, $param, $args = [], $info = []);
    
    public function find($resource, $cond, $fields);
    public function first($resource, $pkey, $fields = '');
    public function insert($resource, $data);
    public function update($resource, $data, $pkey);
    public function delete($resource, $pkey);
    public function getLastID();
    
    public function getSchema();
    public function getLib();
}
