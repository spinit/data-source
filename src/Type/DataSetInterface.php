<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Type;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface DataSetInterface
{
    public function isOpen();
    
    /**
     * Chiude la sorgente dati
     */
    public function close();
    
    /**
     * Restiuisce la posizione del record corrente
     */
    public function position();
    
    /**
     * Restituisce le informazioni associate al dataset
     */
    public function getMetadata($type = '');
    
    /**
     * Restituisce il primo e chiude il dataset
     */
    public function first($field = '');
    
    /**
     * Restituisce tutti i dati come Array()
     */
    public function getAll();
    
    /**
     * Restituisce un iteratore con chiave=>valore
     */
    public function getList();
}
