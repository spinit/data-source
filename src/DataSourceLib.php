<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource;

use Spinit\DataSource\DataSource;
use Spinit\UUIDO;
use Spinit\Util;

/**
 * Description of DataSourceLib
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSourceLib
{
    private $DS;
    private $counter;
    private $commandList = [];
    private $commandLast = [];
    private $debug = 0;
    
    public function __construct($DS)
    {
        $this->DS = $DS;
        $this->setCounter(new UUIDO\Maker(UUIDO\randHexToday()));
    }
    
    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }
    public function getDebug()
    {
        return $this->debug;
    }
    public function getDataSource()
    {
        return $this->DS;
    }
    
    
    /**
     * Imposta il counter che genererà gli ID binari
     * 
     * @param \Spinit\UUIDO\Maker $counter
     */
    public function setCounter(UUIDO\Maker $counter)
    {
        $this->counter = $counter;
    }
    
    /**
     * 
     * @return UUIDO\UUIDO;

     */
    public function getCounter()
    {
        return $this->counter;
    }

    protected function normalizeValue($field, &$params, $matches, $k, $sql)
    {
        // non vuole gli apici finali?
        $strip = ($field{0} == ':');
        $field = $strip ? substr($field, 1) : $field;
        // è un esadecimale?
        $at = ($field{0} == '@');
        $field = $at ? substr($field, 1) : $field;
        // è una lista di valori?
        $lst = ($field{0} == '+');
        $field = $lst ? substr($field, 1) : $field;
        foreach(explode('.', $field) as $ff) {
            if (array_key_exists($ff, $params)) {
                if ($params[$ff] === null) {
                    $value = null;
                    break;
                }
                if (is_array($params[$ff])) {
                    $value = $params[$ff];
                    $params = &$params[$ff];
                    continue;
                }
                if ($at) {
                    $value = $this->getDataSource()->unhex($params[$ff]);
                } else {
                    $value = $this->getDataSource()->quote($params[$ff]);
                    if ($strip) {
                        $value = substr($value, 1, -1);
                    }
                }
                unset($params[$ff]);
            } else {
                // se un valore non è definito allora viene impostato come stringa vuota
                // per metterlo a null occorre impostarlo con il valore [null]
                $value = "''";
            }
            break;
        }
        if ($value === null) {
            if ($lst) {
                $value = $this->getDataSource()->quote('');
            } else {
                $value = 'NULL';
            }
        } else if (is_array($value)) {
            if ($lst) {
                $ll = [];
                foreach($value as $val) {
                    $ll []= $this->getDataSource()->quote($val);
                }
                $value = implode(', ', $ll);
            } else {
                $value = json_encode($value, JSON_UNESCAPED_UNICODE);
            }
        }
        $sql = str_replace($matches[$k], $value, $sql);
        return $sql;
    }

    
    /**
     * I parametri presenti nelle query vengono fuse con i diversi argomenti passati
     * @param type $sqlraw
     * @param type $param
     * @param type $args
     * @param type $info
     * @return type
     */
    public function mergeParam($sqlraw, $param = [], $args = [], $info = [])
    {
        $args = $args ? $args : [];
        $sqlraw = $this->getSql($sqlraw);
        // sostituzione nome database
        $sql = $this->getDataSource()->trigger('normalizeQuery', $sqlraw);
        if (!$sql or !is_string($sql)) {
            $sql = $sqlraw;
        }
        if (!is_array($sql)) {
            $sqls = [$sql];
        } else {
            $sqls = $sql;
        }
        $conf = ['param' => '/\{\{([^ ]+)\}\}/', 'args' => '/\{\[([^ ]+)\]\}/', 'info'=>'/\{\(([^ ]+)\)\}/'];
        foreach($conf as $var => $pattern) {
            foreach($sqls as $idx => $sql) {
                preg_match_all($pattern, $sql, $matches);
                foreach ($matches[1] as $k => $name) {
                    $sql = $this->normalizeValue($name, $$var, $matches[0], $k, $sql);
                }
                // sostituzione valorizzazioni NULL
                $sql = str_replace(array("!= NULL", "<> NULL"), array(' IS NOT NULL', ' IS NOT NULL'), $sql);
                $sql = str_replace(array("= NULL"), array(' IS NULL'), $sql);
                $sqls[$idx] = $sql;
            }
        }
        
        
        // se nei parametri ci sono rimasti elementi array ... vengono tolti
        foreach($param as $key => $value) {
            if (is_array($value)) {
                unset($param[$key]);
            }
        }
        // viene restituita la query pronta per l'esecuzione con l'insieme di parametri che sono affidati al driver del DB
        return array($sqls, $param);
    }
    
    public function getSql($query, $type = '')
    {
        if (is_string($query)) {
            return $query;
        }
        if (isset($query['query'])) {
            $query = $query['query'];
        }
        // query specifica per il tipo database utilizzato
        $sql  = Util\arrayGet($query, $typ ?: $this->getType());
        Assert::notEmpty($sql, ($typ ?: $this->getType()) . " : Query not found");
        return $sql;
    }
    public function getCommandLast()
    {
        return $this->commandLast;
    }
    public function getCommandList($reset = false)
    {
        $commands = $this->commandList;
        if ($reset) {
            $this->commandList = [];
        }
        return $commands;
    }
    public function addCommand($command)
    {
        $this->commandLast = $command;
        if ($this->debug) {
            $this->commandList[] = $command;
        }
    }
    public function mapArrayInString ($value)
    {
        if (is_array($value)) {
            return json_encode($value, JSON_UNESCAPED_UNICODE);
        }
        return $value;
    }
}
