<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource;

use Spinit\DataSource\Type\DataListInterface;
/**
 * Description of DataListArray
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataListArray implements DataListInterface
{
    private $list;
    
    public function __construct ($data)
    {
        if (is_array($data)) {
            $data = new \ArrayObject($data);
        }
        $this->list = $data->getIterator();
    }
    
    public function close() 
    {
        if ($this->list) {
            $this->list = null;
        }
    }

    public function current()
    {
        return $this->list->current();
    }

    public function first()
    {
        $data = $this->current();
        $this->close();
        return $data;
    }

    public function key()
    {
        return $this->list->key();
    }

    public function next()
    {
        return $this->list->next();
    }

    public function rewind()
    {
        return $this->list->rewind();
    }

    public function valid()
    {
        if ($this->list) {
            return $this->list->valid();
        }
        return false;
    }

}
