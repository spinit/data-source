<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource;


use Spinit\Util;
use Spinit\DataSource\Type\DataSourceInterface;
use Webmozart\Assert\Assert;

/**
 * Description of DataSource
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class DataSource implements DataSourceInterface
{
    use Util\ParamTrait;
    
    /**
     * protocolli registrati
     * @var []
     */
    private static $register = [];
    
    /**
     * oggetti già instanziati
     * @var []
     */
    private static $datasource = [];
    private $info;
    private $schema = '';
    /**
     *
     * @var Spinit\UUIDO\Maker
     */
    private $counter;
    
    use \Spinit\Util\TriggerTrait;
    
    /**
     * Al costruttore viene la lista dei parametri impostati sul maker in fase di registrazione del protocollo
     * @param type $param
     */
    public function __construct($param = [])
    {
        $this->setParam($param);
    }
    
    /**
     * Ad INIT viene passato la configurazione per l'istanza con i relativi parametri
     * @param type $info
     * @param type $param
     */
    public function init($info, $param = [])
    {
        $this->info = explode(':', $info);
        $this->setParam($param);
        $this->setLib(new DataSourceLib($this));
    }
    
    abstract public function connect();
    abstract public function getCore();
    
    /**
     * Funzioni di libreria generiche
     * @param \Spinit\DataSource\DataSourceLib $DS
     */
    protected function setLib(DataSourceLib $DS)
    {
        $this->lib = $DS;
    }
    
    /**
     * 
     * @return \Spinit\DataSource\DataSourceLib
     */
    public function getLib()
    {
        return $this->lib;
    }
    
    public static function register($protocol, $maker, $param = [])
    {
        self::$register[$protocol] = ['maker'=>$maker, 'param'=>$param];
    }
    
    public static function get($info, $param = [])
    {
        $protocol = strtok($info, ':');
        $conf = Util\arrayGetAssert(self::$register, $protocol, 'Protocollo non impostato : '.$protocol);
        if (!array_key_exists($info, self::$datasource)) {
            $ds = Util\getInstance($conf['maker'], $conf['param']);
            Assert::isInstanceOf($ds, Util\getClassPath(__NAMESPACE__.':Type:DataSourceInterface'));
            $ds->init($info, $param);
            self::$datasource[$info] = $ds;
        }
        return self::$datasource[$info];
    }

    public function setSchema($name)
    {
        $this->schema = $name;
    }
    public function getSchema()
    {
        return $this->schema;
    }
    
    public function getInfo()
    {
        $args = func_get_args();
        if (!count($args)) {
            return $this->info;
        }
        return Util\arrayGet($this->info, $args[0]);
    }
    
    public function getType()
    {
        return $this->getInfo(0);
    }
    
}
