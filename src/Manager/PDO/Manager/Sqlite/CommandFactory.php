<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Manager\Sqlite;

use Spinit\DataSource\Manager\PDO\CommandFactory as PdoCommandFactory;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of CommandFactory
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CommandFactory extends PdoCommandFactory
{
    /**
     * Restituisce lo schema della tabella richiesta
     * @param string $name
     */
    public function getStruct($table)
    {
        $info = ['name'=>$table, 'pkey'=>[], 'fields'=>[]];
        $fieldCount = 0;
        $sql = "PRAGMA table_info({{table}})";
        foreach($this->getDataSource()->load($sql, ['table'=>$table])->getAll() as $rec) {
            $fieldCount += 1;
            $type = Util\arrayGet($rec, 'type', 'text');
            @preg_match_all("/(\w+)(\((\w+)\))?/", $type, $LVar, PREG_PATTERN_ORDER);
            $field = [
                'type' => $LVar[1][0],
                'size' => $LVar[3][0],
                'pkey' => Util\arrayGet($rec, 'pk'),
                'notnull' => $rec['notnull']=='1'?'1':'',
                'default' => $rec['dflt_value'],
                'incval'=>''
            ];
            $this->tuning($field);
            if ($field['pkey'] == '1') {
                $info['pkey'][] = $rec['name'];
            }
            $info['fields'][$rec['name']] = $field;
        }
        if (!$fieldCount) {
            throw new NotFoundException('Table non found : '.$table);
        }
        // TODO SHOW INDEX
        return $info;
    }
    
    private function tuning(&$field)
    {
        switch($field['type']) {
            case 'varchar' :
                if ($field['default'] == 'NULL') {
                    $field['default'] = null;
                } else if (substr($field['default'], 0, 1) == "'") {
                    $field['default'] = substr(str_replace("''", "'", $field->get('default')), 1, -1);
                }
                break;
            case 'datetime':
                if ($field['default']) {
                    $field['default'] = "({$field['default']})";
                }
                break;
            case 'binary':
                if ($field['size']=='16') {
                    $field['type'] = 'uuid';
                    $field['size'] = '';
                }
                break;
        }
    }

    public function upgradeColumn($table, $field, $conf) {
        
    }

}
