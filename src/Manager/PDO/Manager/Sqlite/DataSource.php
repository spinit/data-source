<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Manager\Sqlite;

use Spinit\DataSource\Manager\PDO\DataSource as PdoDataSource;
use Spinit\DataSource\Manager\PDO\Manager\Sqlite\CommandFactory;
use Spinit\Util;
/**
 * Description of DataSource
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSource extends PdoDataSource
{
    /**
     * Inizializza una nuova connessione
     * @return \PDO
     */
    protected function makePdo()
    {
        $str = implode(':', $this->getInfo());
        $pdo = new \PDO($str);
        $pdo->sqliteCreateFunction('md5', function ($str) {return md5($str); }, 1);
        return $pdo;
    }

    protected function makeCommandFactory()
    {
        return new CommandFactory($this);
    }
    public function check($name)
    {
        $conf = explode('.', $name);
        $sql = "SELECT name FROM sqlite_master WHERE type = 'table' and name = {{name}}";
        $rec = $this->load($sql, ['name' => $conf[0]])->first();
        $ret = Util\arrayGet($rec, 'name') == $conf[0];
        if (count($conf)>1) {
            $struct = $this->getCommandFactory()->getStruct($conf[0]);
            return Util\arrayGet($struct['fields'], $conf[1]) != '';
        }
        return $ret;
    }

    public function quote($str  , $wrap = true)
    {
        $ret =  str_replace("'", "''", $str);
        if ($wrap) {
            $ret = "'{$str}'";
        }
        return $ret;
    }

    public function hex($str)
    {
        return "hex('{$str}')";
    }

    public function unhex($str)
    {
        return "x'{$str}'";
    }

}
