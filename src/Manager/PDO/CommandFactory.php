<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO;

use Spinit\DataSource\Manager\PDO\DataSource;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

/**
 * Factory degli oggetti command del datasource
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class CommandFactory
{
    /**
     *
     * @var DataSource
     */
    private $DS;
    
    public function __construct(DataSource $DS)
    {
        $this->DS = $DS;
    }
    
    public function __call($name, $args)
    {
        if (substr($name, 0, 4) != 'func') {
            throw new NotFoundException($name);
        }
        $className = __NAMESPACE__.':Command:Method'.substr($name, 4);
        array_unshift($args, $this->getDataSource());
        array_unshift($args, $className);
        return call_user_func_array("Spinit\Util\getInstance", $args);
    }
    public function getDataSource()
    {
        return $this->DS;
    }
    abstract public function getStruct($name);
    abstract public function upgradeColumn($table, $field, $conf);
    
    public function addColumn($table, $field, $conf)
    {
        $type = $conf['type'];
        if (Util\arrayGet($conf, 'size')) {
            $type .= "({$conf['size']})";
        }
        $cmd = "ALTER TABLE {$table} ADD COLUMN {$field} {$type}";
        $this->getDataSource()->exec($cmd);
    }
    public function createTable($struct)
    {
        $struct['pkey'] = isset($struct['pkey']) ? $struct['pkey']:[];
        $sql[] = "CREATE TABLE {$struct['name']} (";
        $fields = [];
        foreach($struct['fields'] as $name => $field) {
            $row = "    {$name} {$field['type']}";
            if ($size = Util\arrayGet($field, 'size')) {
                $row.= "({$size})";
            }
            if (Util\arrayGet($field, 'pkey') and !in_array(Util\arrayGet($field, 'pkey'), $struct['pkey'])) {
                $struct['pkey'][] = $name;
            }
            $fields[] = $row;
        }
        if(count($struct['pkey'])) {
            $fields[] = "    PRIMARY KEY (".implode(', ', $struct['pkey']).")";
        }
        $sql[] = implode(','.PHP_EOL, $fields);
        $sql[] = ')';
        $cmd = implode(PHP_EOL, $sql);
        $this->getDataSource()->exec($cmd);
    }
}
