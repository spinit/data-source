<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Command;

use Spinit\DataSource\Manager\PDO\Command;
use Spinit\DataSource\Manager\PDO\DataSource;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of MethodCheck
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MethodAlign extends Command
{
    private $struct;
    
    public function __construct(DataSource $DS, \ArrayAccess $struct)
    {
        parent::__construct($DS);
        $this->struct = $struct;
    }
    
    public function exec()
    {
        try {
            $struct = $this->getFactory()->getStruct($this->struct['name']);
            $this->upgrade($struct);
        } catch (NotFoundException $e) {
            $this->getFactory()->createTable($this->struct);
        }
        return true;
    }
    
    private function upgrade($struct)
    {
        foreach($this->struct['fields'] as $name => $conf) {
            if (!array_key_exists($name, $struct['fields'])) {
                $this->getFactory()->addColumn($this->struct['name'], $name, $conf);
            } else if ($conf != $struct[$name]) {
                $this->getFactory()->upgradeColumn($this->struct['name'], $name, $conf);
            }
        }
    }
}
