<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Command;

use Spinit\DataSource\Manager\PDO\DataSource;
use Spinit\DataSource\Manager\PDO\Command;
use Spinit\Util;

/**
 * Description of MethodFirst
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MethodFirst extends Command
{
    private $resource;
    private $pkey;
    private $fields;
    
    public function __construct(DataSource $DS, $resource, $pkey, $fields = '') {
        parent::__construct($DS);
        $this->resource = $resource;
        $this->pkey = is_array($pkey) ? $pkey : ['id' => $pkey];
        $this->fields = Util\nvl($fields,'*');
    }
    public function exec()
    {
        $p = [];
        $d = [];
        foreach($this->pkey as $nme => $val) {
            $p[] = "{$nme} = :{$nme}";
            $d[$nme] = $val;
        }
        return $this->getDataSource()->load("select ".$this->fields." from ".$this->resource. " where ".implode(' and ', $p), $d)->first();
    }
}
