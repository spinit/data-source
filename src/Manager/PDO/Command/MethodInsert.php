<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Command;
use Spinit\DataSource\Manager\PDO\Command;
use Spinit\DataSource\Manager\PDO\DataSource;
use Spinit\Util;

/**
 * Description of MethodInsert
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MethodInsert extends Command
{
    private $resource;
    private $data;
    public function __construct(DataSource $DS, $resource, $data) {
        parent::__construct($DS);
        $this->resource = $resource;
        $this->data = $data;
    }
    public function exec()
    {
        $d = []; $v = []; $f = [];
        foreach($this->data as $nme => $val) {
            $f[] = $nme;
            $v[] = ':'.$nme;
            $d[$nme] = $val;
        }
        $sql = "INSERT INTO {$this->resource} (".implode(', ', $f).") values (".implode(', ', $v).")";
        return $this->getDataSource()->exec($sql, $d);
    }

}
