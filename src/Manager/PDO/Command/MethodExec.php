<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Command;

use Spinit\DataSource\Manager\PDO\Command;
use Spinit\DataSource\Manager\PDO\DataSource;
use Spinit\Util\Error\NotFoundException;
use Spinit\Util\Error\MessageException;
/**
 * Description of MethodCheck
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MethodExec extends Command
{
    private $sqls;
    private $param;
    
    public function __construct(DataSource $DS, $cmd, $param = [], $args = [], $info = [])
    {
        parent::__construct($DS);
        list($this->sqls, $this->param) = $this->getLib()->mergeParam($cmd, $param, $args, $info);
    }
    
    protected function pdoExec($sql, $params)
    {
        $pdo = $this->getDataSource()->getCore();
        if (count($params)) {
            $rs = $pdo->prepare($sql);
            $res = $rs->execute(array_map([$this->getLib(), 'mapArrayInString'], (array) $params));
        } else {
            $res = $pdo->exec($sql);
        }
        return $res;
    }
    
    private function execute($sql, $params)
    {
        $oparams = $params;
        try {
            $sqlLast = ['sql' => trim($sql), 'param'=>$params];
            $start = \time();
            $res = $this->pdoExec($sql, $params);
            $sqlLast['time'] = \time() - $start;
            $this->getLib()->addCommand($sqlLast);
            return $res;
        } catch (\Exception $e) {
            if ($e instanceof NotFoundException) {
                throw $e;
            }
            throw new MessageException(['message'=>$e->getMessage(), 'sql'=>$sql, 'param'=>$oparams]);
        }
        return $res;
    }
    public function exec()
    {
        $this->getDataSource()->connect();
        $ret = null;
        foreach($this->sqls as $sql) {
            $ret = $this->execute($sql, $this->param);
        }
        return $ret;
    }
}
