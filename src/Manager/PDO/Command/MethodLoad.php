<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Command;

use Spinit\DataSource\Manager\PDO\Command;
use Spinit\DataSource\Manager\PDO\DataSource;
use Spinit\Util\Error\NotFoundException;
use Spinit\DataSource\Manager\PDO\DataSet;

/**
 * Description of MethodCheck
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MethodLoad extends MethodExec
{
    protected function pdoExec($sql, $params)
    {
        if (count($params)) {
            $rs = $this->getDataSource()->getCore()->prepare($sql);
            $rs->execute(array_map([$this->getLib(), 'mapArrayInString'], (array) $params));
        } else {
            $rs = $this->getDataSource()->getCore()->query($sql);
        }
        $ds = new DataSet($rs);
        return $ds;
    }
}
