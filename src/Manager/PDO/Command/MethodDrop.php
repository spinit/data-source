<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO\Command;

use Spinit\DataSource\Manager\PDO\Command;
use Spinit\DataSource\Manager\PDO\DataSource;
use Spinit\Util\Error\NotFoundException;
use Spinit\DataSource\Manager\PDO\DataSet;

/**
 * Description of MethodCheck
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MethodDrop extends Command
{
    private $name;
    
    public function __construct(DataSource $DS, $name)
    {
        parent::__construct($DS);
        $this->name = $name;
    }
    public function exec()
    {
        $this->getDataSource()->exec("DROP TABLE IF EXISTS {$this->name}");
    }

}
