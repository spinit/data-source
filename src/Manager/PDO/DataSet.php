<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO;

use Webmozart\Assert\Assert;
use Spinit\Util;
use Spinit\DataSource\Type\DataSetInterface;

/**
 * Description of PdoDataSet
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSet implements DataSetInterface
{
    /**
     *
     * @var \PDOStatement
     */
    private $rs;
    
    private $current;
    
    private $fetchStyle;
    
    private $open = true;
    
    private $position = -1;
    
    private $columns = null;
    
    public function __construct(\PDOStatement $rs, $fetchStyle = \PDO::FETCH_ASSOC) {
        Assert::notNull($rs);
        $this->rs = $rs;
        $this->fetchStyle = $fetchStyle;
        $this->next();
    }
    public function close()
    {
        if ($this->open) {
            $this->open = false;
            $this->rs->closeCursor();
        }
    }

    public function current()
    {
        if ($this->isOpen()) {
            return $this->current;
        }
        return null;
    }
    public function rowCount()
    {
        if ($this->isOpen()) {
            return $this->rs->rowCount();
        }
        return null;
    }
    public function isOpen()
    {
        return $this->open;
    }

    public function key()
    {
        if ($this->valid()) {
            if (isset($this->current['id'])) {
                return array('id' => $this->current['id']);
            } else {
                $kk = array_keys($this->current);
                return [$kk[0]=>$this->current[$kk[0]]];
            }
        }
        return null;
    }

    public function next()
    {
        if ($this->isOpen()) {
            $this->current = $this->rs->fetch($this->fetchStyle);
            if (!$this->valid()) {
                $this->close();
            } else {
                $this->position++;
            }
        }
        return $this->current();
    }

    public function getAll()
    {
        $data = $this->rs->fetchAll($this->fetchStyle);
        if ($this->current()) {
            array_unshift($data, $this->current());
        }
        $this->close();
        return $data;
    }
    public function getList()
    {
        while($data = $this->current()) {
            switch (count($data)) {
                case 1:
                    yield array_shift($data);
                    break;
                case 2:
                    $key = array_shift($data);
                    yield $key => array_shift($data);
                    break;
                default:
                    $key = array_shift($data);
                    yield $key => $this->current();
            }
            $this->next();
        }
    }
    
    /**
     * Torna il primo record e chiude il recordset.
     * Se viene passato un paramentro ... allora viene ritornato il valore del campo ... se esiste.
     * @return type
     */
    public function first($field = '')
    {
        $current = $this->current;
        $this->close();
        $args = func_get_args();
        if (empty($field)) {
            return $current;
        }
        return Util\arrayGetAssert($current, $first, 'Field not found : '.$field);
    }
    public function position()
    {
        return $this->position;
    }

    public function rewind()
    {
    }

    public function valid()
    {
        return !!$this->current;
    }

    public function getMetadata($type = false)
    {
        $info = [];
        foreach(range(0, $this->rs->columnCount() - 1) as $column_index)
        {
            $meta = $this->rs->getColumnMeta($column_index);
            $info[$meta['name']] = ['name'=>$meta['name'], 'label'=>$meta['name'], 'type'=>$this->getType($meta)];
        }
        return $info;
    }
    protected function getType($meta)
    {
        switch($meta['native_type']) {
            default:
                return $meta['native_type'];
        }
    }
}
