<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO;

/**
 * Classe base dei comandi eseguibili
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class Command
{
    /**
     *
     * @var DataSource
     */
    private $DS;
    
    public function __construct(DataSource $DS)
    {
        $this->DS = $DS;
    }
    
    public function getFactory()
    {
        return $this->getDataSource()->getCommandFactory();
    }
    
    public function getLib()
    {
        return $this->getDataSource()->getLib();
    }
    /**
     * 
     * @return DataSource
     */
    public function getDataSource()
    {
        return $this->DS;
    }
    
    abstract function exec();
}
