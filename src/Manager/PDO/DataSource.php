<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\DataSource\Manager\PDO;

use Spinit\DataSource\DataSource as MainDataSource;
use Spinit\DataSource\Manager\PDO\CommandFactory;

/**
 * Description of PDODataSource
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class DataSource extends MainDataSource
{
    private $commandFactory;
    
    private $pdo;
    
    /**
     * Questa classe è un facade per accedere a tutte le classi a cui è damandata la logica di ogni comando
     * @param type $param
     */
    public function __construct($param = array())
    {
        parent::__construct($param);
        $this->setCommandFactory($this->makeCommandFactory());
    }
    
    public function connect()
    {
        if (!$this->pdo) {
            $this->setPDO($this->makePdo());
        }
        return true;
    }
    /**
     * @return CommandFactory Generatore per la Factory per gli oggetti Command
     */
    abstract protected function makeCommandFactory();
    abstract protected function makePdo();
    
    protected function setCommandFactory(CommandFactory $commandFactory)
    {
        $this->commandFactory = $commandFactory;
        return $this;
    }
    
    /**
     * @return CommandFactory Factory per gli oggetti Command
     */
    public function getCommandFactory()
    {
        return $this->commandFactory;
    }
    
    /**
     * router per associare un comando al relativo method factory
     * @param type $name
     * @param type $args
     */
    private function execCommand($name, $args)
    {
        $this->connect();
        $command = call_user_func_array([$this->getCommandFactory(), 'func'.ucfirst($name)],$args);
        return call_user_func([$command, 'exec']);
    }
    
    public function align(\ArrayAccess $struct)
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }
            
    public function insert($resource, $data)
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function update($resource, $data, $pkey)
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }
    
    public function load($query, $param = [], $args = [], $info = [])
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function delete($resource, $pkey)
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function drop($name)
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function exec($query, $param = [], $args = [], $info = [])
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function find($resource, $cond, $fields)
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function first($resource, $pkey, $fields = '')
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function getCommandLast()
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function getCommandList($reset = false)
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }
    public function getLastID()
    {
        return $this->execCommand(__FUNCTION__, func_get_args());
    }

    public function getCore()
    {
        return $this->getPDO();
    }
    
    protected function getPDO()
    {
        return $this->pdo;
    }
    protected function setPDO($pdo)
    {
        $this->pdo = $pdo;
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $this;
    }
    public function getSql($query, $type = '') {
        try {
            return parent::getSql($query, $type);
        } catch (\Exception $e) {
            return parent::getSql($query, 'sql');
        }
    }
}
